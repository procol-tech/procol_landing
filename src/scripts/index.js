'use strict';

const $modal = document.getElementById("js-modal");
const $trigger = document.querySelectorAll("[data-role='js-modal-trigger']");
const $featureTabs = document.querySelectorAll(".features__key");
const $contactForm = document.getElementById("js-contact-form");

$featureTabs.forEach(el => {
    el.addEventListener("click", (e) => {
        const selectedTab = e.target.parentNode;

        $featureTabs.forEach(n => {
            n.classList.remove("features__key--selected");
        });

        el.classList.add("features__key--selected");
    });
});

$trigger.forEach(el => {
    el.addEventListener("click", (e) => {
        $modal.classList.remove("hidden");
    });
})

$modal.addEventListener("click", (e) => {
    if (e.target.id === "js-modal") {
        $modal.classList.add("hidden");
    }
});

$contactForm.addEventListener("submit", (e) => {
    e.preventDefault();

    const el = e.target.elements;
    const data = {
        name: el[0].value,
        company: el[1].value,
        email: el[2].value,
        phone: el[3].value
    };

    firebase.database().ref("leads").push(data);

    $modal.dispatchEvent(new Event("click"));

    e.target.reset();
});

document.querySelectorAll('a[href^="#"]').forEach(anchor => {
    anchor.addEventListener('click', function(e) {
        e.preventDefault();

        document.querySelector(this.getAttribute('href')).scrollIntoView({
            behavior: 'smooth'
        });
    });
});