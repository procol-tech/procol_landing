# procol_landing

> Landing page for Procol

## Technologies

- [**Gulp**](http://gulpjs.com)
- [**Html**](https://developer.mozilla.org/es/docs/HTML/HTML5)
- [**Sass**](http://sass-lang.com)
- [**Babel**](https://babeljs.io)

## Install and Use

### Install

```bash
npm i
```

### Use

```bash
gulp
```

# Author

thinker3197 - ashish@spars.in

# License

The code is available under the **MIT** license.
